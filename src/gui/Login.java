package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

public class Login {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnCadastrar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setAlwaysOnTop(true);
		frame.getContentPane().setBackground(Color.WHITE);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		textField_1.setColumns(10);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setBackground(new Color(72, 209, 204));
		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setBackground(new Color(72, 209, 204));
		btnCadastrar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel lblDigiteSeuEmail = new JLabel("Digite seu e-mail:");
		lblDigiteSeuEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel lblDigiteSuaSenha = new JLabel("Digite sua senha:");
		lblDigiteSuaSenha.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel img_login = new JLabel("");
		img_login.setIcon(new ImageIcon("C:\\Users\\Everton\\eclipse-workspace\\MSITech\\receitas-despesas\\assets\\login.png"));
		
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addGap(109)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblDigiteSeuEmail)
						.addComponent(textField, GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnLogin, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 106, Short.MAX_VALUE)
							.addComponent(btnCadastrar, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
						.addComponent(textField_1, GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE)
						.addComponent(lblDigiteSuaSenha, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE))
					.addGap(102))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(238, Short.MAX_VALUE)
					.addComponent(img_login)
					.addGap(219))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(img_login, GroupLayout.DEFAULT_SIZE, 101, Short.MAX_VALUE)
					.addGap(13)
					.addComponent(lblDigiteSeuEmail)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblDigiteSuaSenha)
					.addGap(4)
					.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(btnLogin, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnCadastrar, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
					.addGap(66))
		);
		frame.getContentPane().setLayout(groupLayout);
		frame.setBackground(new Color(0, 139, 139));
		frame.setBounds(100, 100, 553, 332);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	}
}
